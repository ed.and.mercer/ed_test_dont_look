+++
title = "About"
layout = "about"
description = "Ed Mercer, Data okay-ish"
date = "2020-07-18"
aliases = ["about-us","about-me"]
author = "Ed Mercer"
+++

Data analyst, visualizer and climber. I've been working in data for 5+ years in the gaming and food delivery industries for companies like Square-Enix and Just Eat. This website is a place where I'll run a small blog, post my portfolio and showcase the charity work I do on the side.

![](3x.gif?raw=true)

If you'd like to see more of my work and content, feel free to look around the rest of the site and if you like what you see, get in touch! If you don't, well, thanks for looking anyway.

This lovely website is created using Hugo and this [theme](https://themes.gohugo.io/hugo-future-imperfect-slim/), hosted on gitlab and redirected to my domain - I am absolutely not a webdev but if things look broken let me know and I'll sort it out as best I can.
