---
author: "Ed Mercer"
title: "A dummy's guide to analytics"
date: 2020-07-13T10:14:07+01:00
description: An intro to data analytics, from scratch
tags: [
    "data",
    "analytics"
]
categories: [
    "data"
]
images:  ["img/2020/07/spot4.jpg"]
draft: false
---

# Learning how to data
I'm still building this site and learning about CSS, MD and web dev in general, so for now this will just be a [link](https://docs.google.com/document/d/1smqQL2iZlvIb23ehw_F5fmUJYKiZD3-MLuRkgP3R3eg/edit#heading=h.hf71k6vf5rl) to the existing work which is a google doc. One day this will be fancy though!
