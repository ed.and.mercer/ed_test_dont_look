---
author: "Ed Mercer"
title: "On the nature of :Madcool:"
date: 2020-07-12T15:41:07+01:00
description: A test, while I learn
tags: [
    "meme",
    "test",
]
categories: [
    "meme"
]
draft: false
---
# :Madcool:
This is genuinely a good representation of me and will serve in lieu of an actual CV and portfolio while I get set up.
Please enjoy.

![](/img/main/3x.gif?raw=true)
